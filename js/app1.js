/** PRIMITIVE TYPES IN JAVASCRIPT
 * undefined
 * null
 * Number
 * String
 * Boolean
 * Symbol
 *  */ 


//Never use undefined!
//Dynamic typed language
var myVar = undefined;
console.log(`myVar is: ${myVar}`);


myVar=15;
console.log(`myVar is: ${myVar}`);


myVar='Something else';
console.log(`myVar is: ${myVar}`);



myVar=true;
console.log(`myVar is: ${myVar}`);




//Mechanism - coercion or conversion - when it shows that they are the same values even if they are not
//Use this only if you know what you are doing (if s and comparisons with == - double equals)
var a = 3;
var b = '3';

if(a == b)
{
    console.log('We have the same values');
}
else
{
    console.log('We have different values');
}


//We use this 99% of the time
if(a === b)
{
    console.log('We have the same values');
}
else
{
    console.log('We have different values');
}




var c = undefined; //we can also type it as var c;

if(c)
{
    //Boolean(c)
    console.log('We have something here');
}
else
{
    console.log('We have nothing here');
}



function getDetails(name, surname)
{
    //When a person calls the function without parameters
    name = name || `<Your name here>`;
    surname = surname || `<Your surname here>`;
    
    
    return `${name} ${surname}`;
}


var fullName = getDetails('Tudorache', 'Monica');


fullName = getDetails('Tudorache');
fullName = getDetails();

console.log(fullName);


//Orders of the operators (+, -) - sa ne uitam peste, how does JavaScript treat the objects?